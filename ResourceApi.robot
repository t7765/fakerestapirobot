*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    String
Library    SeleniumLibrary


*** Variables ***
${URL.API}        https://fakerestapi.azurewebsites.net/api/v1/
&{BOOK_10}        ID=10
...               TITLE=Book 10 
...               PAGECOUNT=1000
...               EXCERPT=Lorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\nLorem lorem lorem. Lorem lorem lorem. Lorem lorem lorem.\n
&{NEW_BOOK}       ID=789
...               TITLE=O melhor livro
...               DESCRIPTION=Livro com descricao para teste
...               PAGECOUNT=50
...               EXCERPT=Livro bom
...               PUBLISHDATE=2022-03-03T15:14:10.794Z
&{BOOK_CHANGED}   ID=251
...               TITLE=Titulo Alterado
...               DESCRIPTION=Descricao alterada
...               PAGECOUNT=80
...               EXCERPT=Resumo alterado
...               PUBLISHDATE=2022-03-03T14:50:04.43Z

*** Keywords ***

# AÇÕES
Conectar API
    Create Session    fakeApi    ${URL.API}

Buscar por todos os livros
    ${RESPOSTA}    GET On Session    fakeApi    books
    Set Test Variable    ${RESPOSTA}    #Faz com que essa variavel fique visivel para todos os outros testes

Cadastrar um novo livro
    ${HEADERS}     Create Dictionary     content-type=application/json
    ${RESPOSTA}    POST On Session    fakeApi    books
    ...            data={"id": 789,"title": "O melhor livro","description": "Livro com descricao para teste","pageCount": 50,"excerpt": "Livro bom","publishDate": "2022-03-03T15:14:10.794Z"}
    ...            headers=${HEADERS}
    Set Test Variable    ${RESPOSTA}

Buscar um livro pelo id ${ID.LIVRO}
    ${RESPOSTA}    GET On Session    fakeApi    books/${ID.LIVRO}
    Set Test Variable    ${RESPOSTA}

Editar todas as informações do livro com o id ${ID.LIVRO}
    ${HEADERS}     Create Dictionary     content-type=application/json
    ${RESPOSTA}    Put On Session    fakeApi    books/${ID.LIVRO}
    ...            data={"id": 251,"title": "Titulo Alterado","description": "Descricao alterada","pageCount": 80,"excerpt": "Resumo alterado","publishDate": "2022-03-03T14:50:04.43Z"}
    ...            headers=${HEADERS}
    Set Test Variable    ${RESPOSTA}

Deletar o livro com o id ${ID.LIVRO}
    ${RESPOSTA}    DELETE On Session    fakeApi    books/${ID.LIVRO}
    Set Test Variable    ${RESPOSTA}

# VALIDAÇÕES

Conferir o status code ${CODE}
    Should Be Equal As Strings    ${RESPOSTA.status_code}    ${CODE}

Conferir o reason ${REASON}
    Should Be Equal    ${RESPOSTA.reason}    ${REASON}

Conferir se retornou ${QUANTIDADE} livros
    Length Should Be     ${RESPOSTA.json()}    ${QUANTIDADE}

Conferir se retorna os dados corretos do livro 10
    Dictionary Should Contain Item       ${RESPOSTA.json()}    id             ${BOOK_10.ID}
    Dictionary Should Contain Item       ${RESPOSTA.json()}    title          ${BOOK_10.TITLE}
    Dictionary Should Contain Item       ${RESPOSTA.json()}    pageCount      ${BOOK_10.PAGECOUNT}
    Dictionary Should Contain Item       ${RESPOSTA.json()}    excerpt        ${BOOK_10.EXCERPT}
    Should Not Be Empty    ${RESPOSTA.json()["description"]}
    Should Not Be Empty    ${RESPOSTA.json()["publishDate"]}

Conferir se retorna os dados corretos do livro recém cadastrados
    Dictionary Should Contain Item    ${RESPOSTA.json()}     id            ${NEW_BOOK.ID}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    title          ${NEW_BOOK.TITLE}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    description    ${NEW_BOOK.DESCRIPTION}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    pageCount      ${NEW_BOOK.PAGECOUNT}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    excerpt        ${NEW_BOOK.EXCERPT}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    publishDate    ${NEW_BOOK.PUBLISHDATE}

Conferir se retorna os dados foram alterados com sucesso
    Dictionary Should Contain Item    ${RESPOSTA.json()}     id            ${BOOK_CHANGED.ID}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    title          ${BOOK_CHANGED.TITLE}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    description    ${BOOK_CHANGED.DESCRIPTION}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    pageCount      ${BOOK_CHANGED.PAGECOUNT}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    excerpt        ${BOOK_CHANGED.EXCERPT}
    Dictionary Should Contain Item    ${RESPOSTA.json()}    publishDate    ${BOOK_CHANGED.PUBLISHDATE}

Conferir se o livro foi deletado
    Should Be Empty    ${RESPOSTA.text}
