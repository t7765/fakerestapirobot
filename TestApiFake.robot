*** Settings ***
Resource        ResourceApi.robot
Test Setup      Conectar API

*** Test Cases ***
CT 1: Buscar todos os livros
    Buscar por todos os livros
    Conferir o status code 200
    Conferir o reason OK
    Conferir se retornou 200 livros

CT 2: Buscar por um livro
    Buscar um livro pelo id 10
    Conferir o status code 200
    Conferir o reason OK
    Conferir se retorna os dados corretos do livro 10

CT 3: Cadastrar um novo livro
    Cadastrar um novo livro
    Conferir o status code 200
    Conferir o reason OK
    Conferir se retorna os dados corretos do livro recém cadastrados

CT 4: Editar um livro
    Editar todas as informações do livro com o id 150
    Conferir o status code 200
    Conferir o reason OK
    Conferir se retorna os dados foram alterados com sucesso

CT 5: Deletar um livro
    Deletar o livro com o id 25
    Conferir o status code 200
    Conferir o reason OK
    Conferir se o livro foi deletado